#!/bin/bash

cmd="./build/examples/openpose/openpose.bin --face --hand --net_resolution 320x240 --face_net_resolution 240x240"

mkdir -p output

# disable access control for X
xhost +

docker run \
        --rm \
        -it \
        --gpus all \
        -e DISPLAY=$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        --device /dev/video0 \
        --name openpose \
        openpose \
        $cmd

# enable access control for X
xhost -
