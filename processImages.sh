#!/bin/bash

cmd="./build/examples/openpose/openpose.bin --image_dir /images --write_images /output --write_json /output --display 0"

mkdir -p output

docker run \
        --rm \
        -it \
        --gpus all \
        -v $(pwd)/testimages:/images \
        -v $(pwd)/output:/output \
        --name openpose \
        openpose \
        $cmd
