# openpose-docker

Dockerfile to build the excellent [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose) software from CMU.

## Prerequisites

Ensure that you have the latest version of Docker installed. For versions prior to 19.03, you may need to install `nvidia-docker`. Read more [here](https://github.com/NVIDIA/nvidia-docker).

## Getting Started

Build the image from Dockerfile

```
$ docker build . -t openpose
```

To run the container (name given: `openpose`), use the following commmands -

Clean previous
```
$ docker stop openpose && docker rm openpose
```

run simple
```
$ docker run -it \
        --gpus all \
        --name openpose \
        openpose
```
Now, you should be inside the interactive docker container, i.e.  
`root@<hash>:/openpose# `



_Note_: Remove the `--gpus all` flag if you want to run the container without GPUs.

_Note_: check also the convenience scripts `processImages.sh` and `realtimeWebcam.sh` for more options.

_Note_: [demo flag inspiration](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/demo_overview.md).

### Supports

1. CUDA 10
2. cuDNN 7
3. Python 3.5.2 (will be 3.7 soon)

_Note_: During build the newest openpose is fetched from github. Though releases in the near future should most likely work as well - this has been tested with version 1.6.0, specifically commit `b5bffe18a8021f5f3ed98f19441b658647d9a8c3`.
